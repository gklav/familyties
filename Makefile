DC=docker-compose

start:
	$(DC) up

build:
	$(DC) up --build --remove-orphans

stop:
	$(DC) down

clean:
	docker system prune
