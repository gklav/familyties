# familyties

![NestJS](https://img.shields.io/badge/v10.0.0-grey?logo=nestjs&label=nestjs&labelColor=red&style=for-the-badge)
![NodeJS](https://img.shields.io/badge/v20.5-grey?logo=nodedotjs&logoColor=white&label=node&labelColor=green&style=for-the-badge)
![Postgresql](https://img.shields.io/badge/v15.4-grey?logo=postgresql&logoColor=white&label=postgresql&labelColor=blue&style=for-the-badge)
![Docker](https://img.shields.io/badge/docker-blue?logo=docker&logoColor=white&style=for-the-badge)

A simple REST API to manage a family tree.

## Authors

[![gklav](https://img.shields.io/badge/gitlab-blue?logo=gitlab&label=gklav&style=for-the-badge)](https://gitlab.com/gklav)

## Features

- API :

  - Create/delete an account (**user**)
  - Create/manage a **family**
  - Manage **individuals** inside a **family**

  **_Coming next_** :

  - _OpenAPI documentation_
  - _CI/CD_
  - _Share a **family** with another **user** !_
  - _User interface (front end)_

## Quick Start

### Technical specifications

- Docker & docker-compose
- NestJS (NodeJS)
- PostgreSQL

### Run Locally

Clone the project

```bash
  git clone https://gitlab.com/gklav/familyties
```

Go to the project directory

```bash
  cd my-project
```

Add .env file (see [.env](##.env) section)\
Run the application

```bash
  make build
```

### .env

To run this project, you will need to add the following environment variables to your .env file

`POSTGRES_HOST`: string

`POSTGRES_PORT`: integer

`POSTGRES_USER`: string

`POSTGRES_PASSWORD`: string

`POSTGRES_AUTOLOADENTITIES`: boolean

`POSTGRES_SYNCHRONIZE`: boolean (false for production)

`JWT_SECRET`: string (false for production)

`JWT_EXPIRATION`: string _(eg:_ 3600s _or_ 1h _)_

## Functional specifications

### Family

A **family** is a gathering of **individuals**. Only the **owner** can access a **family**.

### Individual

An **individual** is a person with the following properties :

- id
- firstName
- lastName
- dateOfBirth
- dateOfDeath
- gender

An **individual** has up to two parents, refering to two other individuals.
