import { INestApplication } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { AuthModule } from '../src/auth/auth.module';
import { PostgresService } from '../src/config/database/postgres/postgres.service';

describe('Auth - /auth (e2e)', () => {
  const user = {
    username: 'auth',
    password: 'auth',
  };

  const randomPayload = {
    random_field: 'random_payload',
  };

  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useClass: PostgresService,
        }),
        AuthModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('Create a new user [POST /auth/register]', () => {
    return request(app.getHttpServer())
      .post('/auth/register')
      .send(user)
      .expect(201)
      .then(({ text }) => {
        expect(text).toEqual('User created');
      });
  });

  it('Fail to create a new user: 409 Conflict (username unavailable) [POST /auth/register]', () => {
    return request(app.getHttpServer())
      .post('/auth/register')
      .send(user)
      .expect(409);
  });

  it('Fail to create a new user: 400 Bad Request (wrong or missing parameters) [POST /auth/register]', () => {
    return request(app.getHttpServer())
      .post('/auth/register')
      .send(randomPayload)
      .expect(400);
  });

  it('Log a user in and return a jwt [POST /auth/login]', () => {
    return request(app.getHttpServer())
      .post('/auth/login')
      .send(user)
      .expect(200)
      .then(({ body }) => {
        expect(body.access_token).toBeDefined;
      });
  });

  it('Fail to log a user in: 401 Unauthorized (wrong or missing parameters) [POST /auth/login]', () => {
    return request(app.getHttpServer())
      .post('/auth/login')
      .send({})
      .expect(401);
  });

  it('Fail to log a user in: 401 Unauthorized (wrong password) [POST /auth/login]', () => {
    return request(app.getHttpServer())
      .post('/auth/login')
      .send({ ...user, password: '' })
      .expect(401);
  });

  it('Fail to log a user in: 400 Bad Request (user does not exist) [POST /auth/login]', () => {
    return request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'John', password: 'password' })
      .expect(400);
  });

  afterAll(async () => {
    await app.close();
  });
});
