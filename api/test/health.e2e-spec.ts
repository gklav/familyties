import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { HealthModule } from '../src/health/health.module';

describe('HealthController E2E Test', () => {
  let app: INestApplication;
  const healthService = { getHealth: () => 'Ok' };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [HealthModule],
    })
      .overrideProvider(healthService)
      .useValue(healthService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/GET health', () => {
    return request(app.getHttpServer())
      .get('/health')
      .expect(200)
      .expect(healthService.getHealth());
  });

  afterAll(async () => {
    await app.close();
  });
});
