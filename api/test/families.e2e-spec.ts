import { INestApplication } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { v4 as uuidv4 } from 'uuid';
import { AppModule } from '../src/app.module';
import { PostgresService } from '../src/config/database/postgres/postgres.service';
import { User } from '../src/users/user.entity';

describe('Families - /families (e2e)', () => {
  const userOwner: User = {
    username: 'ownerFamily',
    password: 'password',
  };

  const userNotOwner: User = {
    username: 'notOwnerFamily',
    password: 'password',
  };

  let jwtUserOwner: string;
  let jwtUserNotOwner: string;

  const familyReq: any = {
    name: 'My family',
  };

  const familyRes: any = {
    ...familyReq,
    owner: {
      username: userOwner.username,
    },
  };

  const randomId = uuidv4();

  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useClass: PostgresService,
        }),
        AppModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    // Get JWT for userNotOwner
    await request(app.getHttpServer()).post('/auth/register').send(userOwner);
    await request(app.getHttpServer())
      .post('/auth/login')
      .send(userOwner)
      .then(({ body }) => {
        jwtUserOwner = body.access_token;
      });

    // Get JWT for userNotOwner
    await request(app.getHttpServer())
      .post('/auth/register')
      .send(userNotOwner);
    await request(app.getHttpServer())
      .post('/auth/login')
      .send(userNotOwner)
      .then(({ body }) => {
        jwtUserNotOwner = body.access_token;
      });
  });

  it('Create a new family [POST /families]', () => {
    return request(app.getHttpServer())
      .post('/families')
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .send(familyReq)
      .expect(201)
      .then(({ body }) => {
        familyRes.id = body.id;
        expect(body).toEqual(familyRes);
      });
  });

  it('Fail to create a new family: 400 Bad Request (wrong or missing parameters) [POST /families]', () => {
    return request(app.getHttpServer())
      .post('/families')
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .send({})
      .expect(400);
  });

  it('Fail to create a new family: 401 Unauthorized (JWT missing) [POST /families]', () => {
    return request(app.getHttpServer())
      .post('/families')
      .set('Authorization', 'bearer ')
      .send({})
      .expect(401);
  });

  it('Fetch a family details [GET /families/:id]', () => {
    return request(app.getHttpServer())
      .get('/families/' + familyRes.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(200)
      .then(({ body }) => {
        expect(body).toEqual(familyRes);
      });
  });

  it('Fail to fetch a family details: 401 Unauthorized (JWT missing) [GET /families/:id]', () => {
    return request(app.getHttpServer())
      .get('/families/' + familyRes.id)
      .set('Authorization', 'bearer ')
      .expect(401);
  });

  it('Fail to fetch a family details: 403 Forbidden (User is not owner) [GET /families/:id]', () => {
    return request(app.getHttpServer())
      .get('/families/' + familyRes.id)
      .set('Authorization', 'bearer ' + jwtUserNotOwner)
      .expect(403);
  });

  it('Fail to fetch a family details: 404 Not Found (Family not found) [GET /families/:id]', () => {
    return request(app.getHttpServer())
      .get('/families/' + randomId)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(404);
  });

  it('Update a family details [PUT /families/:id]', () => {
    return request(app.getHttpServer())
      .put('/families/' + familyRes.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .send({ name: 'family' })
      .expect(200)
      .then(({ body }) => {
        expect(body).toEqual({ id: familyRes.id, name: 'family' });
      });
  });

  it('Fail to update a family details: 401 Unauthorized (JWT missing) [PUT /families/:id]', () => {
    return request(app.getHttpServer())
      .put('/families/' + familyRes.id)
      .set('Authorization', 'bearer ')
      .send({ name: 'family' })
      .expect(401);
  });

  it('Fail to update a family details: 400 Bad Request (wrong parameters) [PUT /families/:id]', () => {
    return request(app.getHttpServer())
      .put('/families/' + familyRes.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .send({ id: 'family' })
      .expect(400);
  });

  it('Fail to update a family details: 403 Forbidden (User is not owner) [PUT /families/:id]', () => {
    return request(app.getHttpServer())
      .put('/families/' + familyRes.id)
      .set('Authorization', 'bearer ' + jwtUserNotOwner)
      .send({ name: 'family' })
      .expect(403);
  });

  it('Fail to update a family details: 404 Not Found (Family not found) [PUT /families/:id]', () => {
    return request(app.getHttpServer())
      .put('/families/' + randomId)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .send({ name: 'family' })
      .expect(404);
  });

  it('Fail to delete a family : 401 Unauthorized (JWT missing) [GET /families/:id]', () => {
    return request(app.getHttpServer())
      .delete('/families/' + familyRes.id)
      .set('Authorization', 'bearer ')
      .expect(401);
  });

  it('Fail to delete a family : 403 Forbidden (User is not owner) [GET /families/:id]', () => {
    return request(app.getHttpServer())
      .delete('/families/' + familyRes.id)
      .set('Authorization', 'bearer ' + jwtUserNotOwner)
      .expect(403);
  });

  it('Fail to delete a family: 404 Not Found (Family not found) [GET /families/:id]', () => {
    return request(app.getHttpServer())
      .delete('/families/' + randomId)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(404);
  });

  it('Delete a family [DELETE /families/:id]', () => {
    return request(app.getHttpServer())
      .delete('/families/' + familyRes.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(200);
  });

  afterAll(async () => {
    await app.close();
  });
});
