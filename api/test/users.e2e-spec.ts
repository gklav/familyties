import { INestApplication } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { AuthModule } from '../src/auth/auth.module';
import { PostgresService } from '../src/config/database/postgres/postgres.service';
import { UsersModule } from '../src/users/users.module';

describe('Auth - /users/me (e2e)', () => {
  const user = {
    username: 'user',
    password: 'user',
  };

  let jwt: string;

  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useClass: PostgresService,
        }),
        UsersModule,
        AuthModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    await request(app.getHttpServer()).post('/auth/register').send(user);
    await request(app.getHttpServer())
      .post('/auth/login')
      .send(user)
      .then(({ body }) => {
        jwt = body.access_token;
      });
  });

  it('Get current user from JWT [GET /users/me]', () => {
    return request(app.getHttpServer())
      .get('/users/me')
      .set('Authorization', 'bearer ' + jwt)
      .expect(200)
      .then(({ body }) => expect(body).toEqual({ username: user.username }));
  });

  it('Fail to get current user from JWT: 401 Unauthorized (Authorization header missing) [GET /users/me]', () => {
    return request(app.getHttpServer()).get('/users/me').expect(401);
  });

  it('Fail to get current user from JWT: 401 Unauthorized (Invalid bearer token) [GET /users/me]', () => {
    return request(app.getHttpServer())
      .get('/users/me')
      .set('Authorization', 'bearer random-value')
      .expect(401);
  });

  it('Fail to get current user from JWT: 401 Unauthorized (Invalid authentication method) [GET /users/me]', () => {
    return request(app.getHttpServer())
      .get('/users/me')
      .set('Authorization', 'basic random-value')
      .expect(401);
  });

  it('Fail to delete current user from JWT: 401 Unauthorized (Authorization header missing) [GET /users/me]', () => {
    return request(app.getHttpServer()).delete('/users/me').expect(401);
  });

  it('Fail to delete current user from JWT: 401 Unauthorized (Invalid bearer token) [GET /users/me]', () => {
    return request(app.getHttpServer())
      .delete('/users/me')
      .set('Authorization', 'bearer random-value')
      .expect(401);
  });

  it('Fail to delete current user from JWT: 401 Unauthorized (Invalid authentication method) [GET /users/me]', () => {
    return request(app.getHttpServer())
      .delete('/users/me')
      .set('Authorization', 'basic random-value')
      .expect(401);
  });

  it('Delete current user from JWT [GET /users/me]', () => {
    return request(app.getHttpServer())
      .get('/users/me')
      .set('Authorization', 'bearer ' + jwt)
      .expect(200);
  });

  afterAll(async () => {
    await app.close();
  });
});
