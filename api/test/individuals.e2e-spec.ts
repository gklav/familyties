import { INestApplication } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UUID } from 'crypto';
import * as request from 'supertest';
import { v4 as uuidv4 } from 'uuid';
import { AppModule } from '../src/app.module';
import { PostgresService } from '../src/config/database/postgres/postgres.service';
import { CreateFamilyDto } from '../src/families/dto/create-family.dto';
import { Family } from '../src/families/family.entity';
import { CreateIndividualDto } from '../src/individuals/dto/create-individual.dto';
import { User } from '../src/users/user.entity';

describe('Individuals - /individuals (e2e)', () => {
  let basePath: string;
  const randomFamilyId = uuidv4();
  const wrongBasePath = '/families/' + randomFamilyId + '/individuals';

  const userOwner: User = {
    username: 'ownerIndividual',
    password: 'password',
  };

  const userNotOwner: User = {
    username: 'notOwnerIndividual',
    password: 'password',
  };

  let jwtUserOwner: string;
  let jwtUserNotOwner: string;

  let family: Family;

  const individual = {
    id: uuidv4(),
    firstName: 'firstName',
    lastName: 'lastName',
    dateOfBirth: new Date('1970-01-01'),
    dateOfDeath: new Date('2023-01-01'),
    gender: 'unknown',
    parents: [],
  };

  const individualChildId: UUID = uuidv4();

  const individualChild = {
    id: individualChildId,
    firstName: 'firstName',
    lastName: 'lastName',
    dateOfBirth: new Date('1970-01-01'),
    dateOfDeath: new Date('2023-01-01'),
    gender: 'unknown',
    parents: [individual],
  };

  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useClass: PostgresService,
        }),
        AppModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    // Get JWT for userOwner
    await request(app.getHttpServer()).post('/auth/register').send(userOwner);
    await request(app.getHttpServer())
      .post('/auth/login')
      .send(userOwner)
      .then(({ body }) => {
        jwtUserOwner = body.access_token;
      });

    // Get JWT for userNotOwner
    await request(app.getHttpServer())
      .post('/auth/register')
      .send(userNotOwner);
    await request(app.getHttpServer())
      .post('/auth/login')
      .send(userNotOwner)
      .then(({ body }) => {
        jwtUserNotOwner = body.access_token;
      });

    // Create family
    await request(app.getHttpServer())
      .post('/families')
      .send({ name: 'My fmily' } as CreateFamilyDto)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .then(({ body }) => {
        family = body;
      });

    basePath = '/families/' + family.id + '/individuals';
  });

  // Post
  it('Create a new idividual [POST /families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .post(basePath)
      .send(individual as CreateIndividualDto)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(201)
      .then(({ body }) => {
        expect(body).toEqual({
          ...individual,
          dateOfBirth: '1970-01-01T00:00:00.000Z',
          dateOfDeath: '2023-01-01T00:00:00.000Z',
        });
      });
  });

  it('Create a new individual with parents [POST families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .post(basePath)
      .send(individualChild as CreateIndividualDto)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(201)
      .then(({ body }) => {
        expect(body).toEqual({
          ...individualChild,
          dateOfBirth: '1970-01-01T00:00:00.000Z',
          dateOfDeath: '2023-01-01T00:00:00.000Z',
          parents: [
            {
              ...individual,
              dateOfBirth: '1970-01-01T00:00:00.000Z',
              dateOfDeath: '2023-01-01T00:00:00.000Z',
              parents: undefined,
            },
          ],
        });
      });
  });

  it('Fail to create a new individual: 400 Bad Request (identical parents) [POST families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .post(basePath)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .send({
        ...individualChild,
        parents: [{ id: individual.id }, { id: individual.id }],
      } as CreateIndividualDto)
      .expect(400);
  });

  it('Fail to create a new individual: 400 Unauthorize (JWT missing) [POST families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .post(basePath)
      .send(individual as CreateIndividualDto)
      .set('Authorization', 'bearer ')
      .expect(401);
  });

  it('Fail to create a new individual: 403 Forbidden (user is not owner) [POST families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .post(basePath)
      .send(individual as CreateIndividualDto)
      .set('Authorization', 'bearer ' + jwtUserNotOwner)
      .expect(403);
  });

  it('Fail to create a new individual: 404 Not found (family does not exist) [POST families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .post(wrongBasePath)
      .send(individual as CreateIndividualDto)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(404);
  });

  it('Fail to create a new individual: 404 Not found (parents do not exist) [POST families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .post(basePath)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .send({
        ...individualChild,
        parents: [{ id: uuidv4() }],
      } as CreateIndividualDto)
      .expect(404);
  });

  // Get all
  it('Fetch all individuals from a family [GET families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .get(basePath)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(200)
      .then(({ body }) => {
        expect(body).toBeDefined();
      });
  });

  it('Fail to fetch all individuals from a family: 401 Unauthorized (JWT missing) [GET families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .get(basePath)
      .set('Authorization', 'bearer ')
      .expect(401);
  });

  it('Fail to fetch all individuals from a family: 403 forbidden (user is not owner) [GET families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .get(basePath)
      .set('Authorization', 'bearer ' + jwtUserNotOwner)
      .expect(403);
  });

  it('Fail to fetch all individuals from a family: 404 Not Found (family does not exist) [GET families/:familyId/individuals]', () => {
    return request(app.getHttpServer())
      .get(wrongBasePath)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(404);
  });

  // Get one
  it('Fetch one individual [GET families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .get(basePath + '/' + individual.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(200)
      .then(({ body }) => {
        expect(body).toEqual({
          ...individual,
          dateOfBirth: '1970-01-01T00:00:00.000Z',
          dateOfDeath: '2023-01-01T00:00:00.000Z',
        });
      });
  });

  it('Fail to fetch one individual: 401 Unauthorized (JWT missing) [GET families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .get(basePath + '/' + individual.id)
      .set('Authorization', 'bearer ')
      .expect(401);
  });

  it('Fail to fetch one individual: 403 forbidden (user is not owner) [GET families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .get(basePath + '/' + individual.id)
      .set('Authorization', 'bearer ' + jwtUserNotOwner)
      .expect(403);
  });

  it('Fail to fetch one individual: 404 Not Found (family does not exist) [GET families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .get(wrongBasePath + '/' + individual.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(404);
  });

  // Put

  it('Update one individual [PUT families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .put(basePath + '/' + individual.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .send({ ...individual, firstName: 'Jack' } as CreateIndividualDto)
      .expect(200)
      .then(({ body }) => {
        expect(body).toEqual({
          ...individual,
          firstName: 'Jack',
          dateOfBirth: '1970-01-01T00:00:00.000Z',
          dateOfDeath: '2023-01-01T00:00:00.000Z',
        });
      });
  });

  it('Update an individual with two identical parents [PUT families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .put(basePath + '/' + individualChild.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .send({
        ...individualChild,
        parents: [{ id: individual.id }, { id: individual.id }],
      } as CreateIndividualDto)
      .expect(400);
  });

  it('Fail to update an individual: 404 Not Found (parent does not exist) [PUT families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .put(basePath + individualChild.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .send({
        ...individualChild,
        parents: [{ id: uuidv4() }],
      } as CreateIndividualDto)
      .expect(404);
  });

  // Delete
  it('Delete one individual [DELETE families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .delete(basePath + '/' + individual.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(200);
  });

  it('Delete one parent individual [DELETE families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .delete(basePath + '/' + individualChild.id)
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(200);
  });

  it('Fail to delete an individual: 404 Not Found (individual does not exist) [DELETE families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .delete(basePath + '/' + uuidv4())
      .set('Authorization', 'bearer ' + jwtUserOwner)
      .expect(404);
  });

  /*

  it('Update one individual [PUT families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .put(basePath + '/' + individual.id)
      .send({ ...individual, firstName: 'Jack' } as CreateIndividualDto)
      .expect(200)
      .then(({ body }) => {
        expect(body).toEqual({
          ...individual,
          firstName: 'Jack',
          dateOfBirth: '1970-01-01T00:00:00.000Z',
          dateOfDeath: '2023-01-01T00:00:00.000Z',
        });
      });
  });

  it('Update an individual with two identical parents [PUT families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .put(basePath + '/' + individualChild.id)
      .send({
        ...individualChild,
        parents: [{ id: individual.id }, { id: individual.id }],
      } as CreateIndividualDto)
      .expect(400);
  });

  it('Update an individual with non existing parents [PUT families/:familyId/individuals/:id]', () => {
    return request(app.getHttpServer())
      .put(basePath + individualChild.id)
      .send({
        ...individualChild,
        parents: [{ id: uuidv4() }],
      } as CreateIndividualDto)
      .expect(404);
  });


*/
  afterAll(async () => {
    await app.close();
  });
});
