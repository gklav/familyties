import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../users/user.entity';
import { IsUUID } from 'class-validator';

@Entity()
export class Family {
  @PrimaryGeneratedColumn('uuid')
  @IsUUID()
  id: string;

  @Column()
  name: string;

  @ManyToOne(() => User, {
    onDelete: 'CASCADE',
  })
  owner: User;
}
