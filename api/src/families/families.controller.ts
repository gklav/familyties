import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  Request,
  UseGuards,
  UseInterceptors,
  ValidationPipe,
} from '@nestjs/common';
import { UUID } from 'crypto';
import { CreateFamilyDto } from './dto/create-family.dto';
import { FamiliesService } from './families.service';
import { Family } from './family.entity';
import { OwnerGuard } from './owner.guard';

@Controller('families')
export class FamiliesController {
  constructor(private familiesService: FamiliesService) {}

  @Post()
  async create(
    @Request() req,
    @Body(ValidationPipe) createFamilyDto: CreateFamilyDto,
  ): Promise<Family> {
    return await this.familiesService.create(
      req.user.username,
      createFamilyDto,
    );
  }

  @UseGuards(OwnerGuard)
  @UseInterceptors(ClassSerializerInterceptor)
  @Get(':familyId')
  async findOne(
    @Param('familyId', ParseUUIDPipe) familyId: UUID,
  ): Promise<Family> {
    const family = await this.familiesService.findOne(familyId);
    return family;
  }

  @UseGuards(OwnerGuard)
  @Put(':familyId')
  async update(
    @Param('familyId', ParseUUIDPipe) id: UUID,
    @Body(ValidationPipe) createFamilyDto: CreateFamilyDto,
  ): Promise<Family> {
    return this.familiesService.update(id, createFamilyDto);
  }

  @UseGuards(OwnerGuard)
  @Delete(':familyId')
  async remove(@Param('familyId', ParseUUIDPipe) id: UUID): Promise<string> {
    await this.familiesService.remove(id);
    return `Removal of family ${id}`;
  }
}
