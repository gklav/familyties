import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Family } from './family.entity';
import { CreateFamilyDto } from './dto/create-family.dto';

@Injectable()
export class FamiliesService {
  constructor(
    @InjectRepository(Family)
    private familiesRepository: Repository<Family>,
  ) {}

  async create(owner: string, familyDto: CreateFamilyDto): Promise<Family> {
    try {
      return await this.familiesRepository.save({
        ...familyDto,
        owner: {
          username: owner,
        },
      });
    } catch (err) {
      throw err;
    }
  }

  async findOne(id: string): Promise<Family> {
    try {
      const result = await this.familiesRepository.findOne({
        where: { id: id },
        relations: { owner: true },
      });
      if (result) {
        return result;
      }
      throw new NotFoundException();
    } catch (err) {
      throw err;
    }
  }

  async update(id: string, familyDto: CreateFamilyDto): Promise<Family> {
    try {
      const result = await this.familiesRepository.findOneBy({ id });

      if (result) {
        try {
          const update = Object.assign(result, familyDto);
          const { id } = await this.familiesRepository.save(update);
          return await this.familiesRepository.findOne({
            where: { id: id },
          });
        } catch (err) {
          throw err;
        }
      }
      throw new NotFoundException();
    } catch (err) {
      throw err;
    }
  }

  async remove(id: string): Promise<DeleteResult> {
    try {
      const result = await this.familiesRepository.findOneBy({ id });
      if (result) {
        await this.familiesRepository.delete(id);
        return;
      }
      throw new NotFoundException();
    } catch (err) {
      throw err;
    }
  }
}
