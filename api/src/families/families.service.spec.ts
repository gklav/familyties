import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UUID } from 'crypto';
import { User } from 'src/users/user.entity';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { FamiliesService } from './families.service';
import { Family } from './family.entity';

const generatedId: UUID = uuidv4();

const user: User = {
  username: 'username',
  password: 'password',
};

const family: Family = {
  id: generatedId,
  name: 'Doe',
  owner: user,
};

const updateFamily: Family = {
  id: generatedId,
  name: 'Dupont',
  owner: user,
};

describe('FamiliesService', () => {
  let familiesService: FamiliesService;
  let familiesRepository: Repository<Family>;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FamiliesService,
        {
          provide: getRepositoryToken(Family),
          useValue: {
            save: jest.fn().mockResolvedValue(family),
            findOneBy: jest.fn().mockResolvedValue(family),
            findOne: jest.fn().mockResolvedValue(family),
            delete: jest.fn(),
          },
        },
      ],
    }).compile();

    familiesService = module.get<FamiliesService>(FamiliesService);
    familiesRepository = module.get<Repository<Family>>(
      getRepositoryToken(Family),
    );
  });

  it('should be defined', () => {
    expect(familiesService).toBeDefined();
  });

  describe('create()', () => {
    it('should successfully insert a family', () => {
      const saveSpy = jest.spyOn(familiesRepository, 'save');
      expect(
        familiesService.create(user.username, { name: 'Doe' }),
      ).resolves.toEqual(family);
      expect(saveSpy).toBeCalledWith({
        name: 'Doe',
        owner: {
          username: user.username,
        },
      });
    });
  });

  describe('findOne()', () => {
    it('should get a specific family', () => {
      expect(familiesService.findOne(generatedId)).resolves.toEqual(family);
    });
  });

  describe('update()', () => {
    it('should update a specific family', () => {
      expect(
        familiesService.update(generatedId, { name: updateFamily.name }),
      ).resolves.toEqual(updateFamily);
    });
  });

  describe('remove()', () => {
    it('should remove a specifc family', async () => {
      const removeSpy = jest.spyOn(familiesRepository, 'delete');
      const retVal = await familiesService.remove(generatedId);
      expect(removeSpy).toBeCalledWith(generatedId);
      expect(retVal).toBeUndefined();
    });
  });
});
