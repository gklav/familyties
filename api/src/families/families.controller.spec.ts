import { Test, TestingModule } from '@nestjs/testing';
import { UUID } from 'crypto';
import { v4 as uuidv4 } from 'uuid';
import { CreateFamilyDto } from './dto/create-family.dto';
import { FamiliesController } from './families.controller';
import { FamiliesService } from './families.service';
import { OwnerGuard } from './owner.guard';

const createFamilyDto: CreateFamilyDto = {
  name: 'Doe',
};

const updateFamilyDto: CreateFamilyDto = {
  name: 'Dupont',
};

const user = {
  username: 'username',
};

const request = {
  user: {
    username: user.username,
  },
};

const generatedId: UUID = uuidv4();

describe('FamiliesController', () => {
  let familiesController: FamiliesController;
  let familiesService: FamiliesService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [FamiliesController],
      providers: [
        FamiliesService,
        {
          provide: FamiliesService,
          useValue: {
            create: jest.fn().mockImplementation(() =>
              Promise.resolve({
                id: generatedId,
                owner: user,
                ...createFamilyDto,
              }),
            ),
            findOne: jest.fn().mockImplementation(() =>
              Promise.resolve({
                id: generatedId,
                owner: user,
                ...createFamilyDto,
              }),
            ),
            update: jest.fn().mockImplementation(() =>
              Promise.resolve({
                id: generatedId,
                owner: user,
                ...updateFamilyDto,
              }),
            ),
            remove: jest.fn(),
          },
        },
      ],
    })
      .overrideGuard(OwnerGuard)
      .useValue({
        canActivte: async () => {
          return true;
        },
      })
      .compile();

    familiesController = app.get<FamiliesController>(FamiliesController);
    familiesService = app.get<FamiliesService>(FamiliesService);
  });

  it('should be defined', () => {
    expect(familiesController).toBeDefined();
  });

  describe('create()', () => {
    it('should create a new family', () => {
      familiesController.create(request, createFamilyDto);
      expect(
        familiesController.create(request, createFamilyDto),
      ).resolves.toEqual({
        id: generatedId,
        owner: user,
        ...createFamilyDto,
      });
      expect(familiesService.create).toHaveBeenCalledWith(
        user.username,
        createFamilyDto,
      );
    });
  });

  describe('findOne()', () => {
    it('should return a specific family', () => {
      expect(familiesController.findOne(generatedId)).resolves.toEqual({
        id: generatedId,
        owner: user,
        ...createFamilyDto,
      });
      expect(familiesService.findOne).toHaveBeenCalledWith(generatedId);
    });
  });

  describe('update()', () => {
    it('should update a specific family', () => {
      familiesService.update(generatedId, updateFamilyDto);
      expect(
        familiesController.update(generatedId, updateFamilyDto),
      ).resolves.toEqual({
        id: generatedId,
        owner: user,
        ...updateFamilyDto,
      });
      expect(familiesService.update).toHaveBeenCalled();
    });
  });

  describe('remove()', () => {
    it('should remove a specific family', () => {
      familiesController.remove(generatedId);
      expect(familiesService.remove).toHaveBeenCalled();
    });
  });
});
