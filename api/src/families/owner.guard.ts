import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { FamiliesService } from './families.service';

@Injectable()
export class OwnerGuard implements CanActivate {
  constructor(private familiesService: FamiliesService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const family = await this.familiesService.findOne(request.params.familyId);

    return family.owner.username == request.user.username;
  }
}
