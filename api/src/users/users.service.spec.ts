import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { UsersService } from './users.service';

const user: User = {
  username: 'username',
  password: 'password',
};

const foundUser: User = {
  username: 'findme',
  password: 'password',
};

describe('UsersService', () => {
  let usersService: UsersService;
  let usersRepository: Repository<User>;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            save: jest.fn().mockResolvedValue(user),
            findOneBy: jest.fn().mockImplementation(async ({ username }) => {
              return username == foundUser.username ? foundUser : null;
            }),
            delete: jest.fn(),
          },
        },
      ],
    }).compile();

    usersService = module.get<UsersService>(UsersService);
    usersRepository = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(usersService).toBeDefined();
  });

  describe('create()', () => {
    it('should return a user', () => {
      expect(usersService.create(user)).resolves.toEqual(user);
    });
  });

  describe('findOne()', () => {
    it('should return a user', () => {
      usersService.findOne(foundUser.username);
      expect(usersService.findOne(foundUser.username)).resolves.toEqual(
        foundUser,
      );
    });
  });

  describe('delete()', () => {
    it('should call delete() from repository', async () => {
      const removeSpy = jest.spyOn(usersRepository, 'delete');
      const retVal = await usersService.delete(user.username);
      expect(removeSpy).toBeCalledWith(user.username);
      expect(retVal).toBeUndefined();
    });
  });
});
