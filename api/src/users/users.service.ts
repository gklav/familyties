import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(userDto: CreateUserDto): Promise<User> {
    const saltRounds = 10;

    if (await this.usersRepository.findOneBy({ username: userDto.username })) {
      throw new ConflictException();
    }

    try {
      const salt = await bcrypt.genSalt(saltRounds);
      const hash = await bcrypt.hash(userDto.password, salt);

      return await this.usersRepository.save({ ...userDto, password: hash });
    } catch (err) {
      throw err;
    }
  }

  async findOne(username: string): Promise<any | undefined> {
    try {
      const result = await this.usersRepository.findOneBy({
        username,
      });

      if (result) {
        return result;
      }

      throw new BadRequestException();
    } catch (err) {
      throw err;
    }
  }

  async delete(username: string) {
    try {
      await this.usersRepository.delete(username);
      return;
    } catch (err) {
      throw err;
    }
  }
}
