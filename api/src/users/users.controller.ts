import { Controller, Delete, Get, Request } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller('users/me')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get()
  async findOne(@Request() req): Promise<any> {
    return req.user;
  }

  @Delete()
  async delete(@Request() req): Promise<string> {
    await this.usersService.delete(req.user.username);

    return 'User deleted';
  }
}
