import { Test, TestingModule } from '@nestjs/testing';
import { User } from '../users/user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

const user: User = {
  username: 'username',
  password: 'password',
};

const request = {
  user: {
    username: user.username,
  },
};

const hash = 'hashed_password';

describe('UsersController', () => {
  let usersController: UsersController;
  let usersService: UsersService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        {
          provide: UsersService,
          useValue: {
            findOne: jest.fn().mockResolvedValue({ ...user, password: hash }),
            delete: jest.fn(),
          },
        },
      ],
    }).compile();

    usersController = app.get<UsersController>(UsersController);
    usersService = app.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(usersController).toBeDefined();
  });

  describe('findOne()', () => {
    it('should return a user without the password property', () => {
      usersController.findOne(request);
      expect(usersController.findOne(request)).resolves.toEqual({
        username: user.username,
      });
    });
  });

  describe('delete()', () => {
    it('should return "User deleted"', () => {
      usersController.delete(request);
      expect(usersController.delete(request)).resolves.toEqual('User deleted');
      expect(usersService.delete).toHaveBeenCalledWith(request.user.username);
    });
  });
});
