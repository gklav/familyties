import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { IndividualsModule } from './individuals/individuals.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostgresService } from './config/database/postgres/postgres.service';
import { HealthModule } from './health/health.module';
import { FamiliesModule } from './families/families.modules';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useClass: PostgresService,
    }),
    ConfigModule.forRoot(),
    HealthModule,
    FamiliesModule,
    IndividualsModule,
    AuthModule,
    UsersModule,
  ],
})
export class AppModule {}
