import {
  ArrayMaxSize,
  IsArray,
  IsDateString,
  IsOptional,
  IsString,
} from 'class-validator';
import { CreateParentDto } from './create-parent.dto';

export class CreateIndividualDto {
  @IsOptional()
  @IsString()
  firstName: string;

  @IsOptional()
  @IsString()
  lastName: string;

  @IsOptional()
  @IsDateString()
  dateOfBirth: Date;

  @IsOptional()
  @IsDateString()
  dateOfDeath: Date;

  @IsOptional()
  @IsString()
  gender: string;

  @IsOptional()
  @IsArray()
  @ArrayMaxSize(2)
  parents: CreateParentDto[];
}
