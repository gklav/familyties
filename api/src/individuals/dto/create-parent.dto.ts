import { IsUUID } from 'class-validator';

export class CreateParentDto {
  @IsUUID()
  id: string;
}
