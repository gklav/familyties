import { Test, TestingModule } from '@nestjs/testing';
import { Individual } from './individual.entity';
import { IndividualsService } from './individuals.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { UUID } from 'crypto';
import { Family } from 'src/families/family.entity';

const familyId: UUID = uuidv4();

const parentIndividualId: UUID = uuidv4();

const parentIndividual = {
  id: parentIndividualId,
  firstName: 'firstName #2',
  lastName: 'lastName #2',
  dateOfBirth: new Date('1940-01-01'),
  dateOfDeath: new Date('1990-01-01'),
  gender: 'unknown',
  parents: [],
};

const individualId: UUID = uuidv4();

const oneIndividual = {
  id: individualId,
  firstName: 'firstName',
  lastName: 'lastName',
  dateOfBirth: new Date('1970-01-01'),
  dateOfDeath: new Date('2023-01-01'),
  gender: 'unknown',
  parents: [parentIndividual],
};

const individualArray = [oneIndividual, parentIndividual];

describe('IndividualService', () => {
  let individualsService: IndividualsService;
  let individualsRepository: Repository<Individual>;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        IndividualsService,
        {
          provide: getRepositoryToken(Individual),
          useValue: {
            findOneBy: jest.fn().mockResolvedValue(oneIndividual),
            findOne: jest
              .fn()
              .mockImplementation(
                (options: { where: { id: string }; relations?: any }) => {
                  let foundIndividual;
                  if (options.where.id === oneIndividual.id) {
                    foundIndividual = oneIndividual;
                  }
                  if (options.where.id === parentIndividual.id) {
                    foundIndividual = parentIndividual;
                  }
                  return Promise.resolve(foundIndividual);
                },
              ),
            find: jest.fn().mockResolvedValue(individualArray),
            save: jest.fn().mockResolvedValue(oneIndividual),
            delete: jest.fn(),
          },
        },
      ],
    }).compile();

    individualsService = module.get<IndividualsService>(IndividualsService);
    individualsRepository = module.get<Repository<Individual>>(
      getRepositoryToken(Individual),
    );
  });

  it('should be defined', () => {
    expect(individualsService).toBeDefined();
  });

  describe('create()', () => {
    it('should successfully insert an individual', () => {
      expect(
        individualsService.create(familyId, {
          firstName: 'firstName',
          lastName: 'lastName',
          dateOfBirth: new Date('1970-01-01'),
          dateOfDeath: new Date('1970-01-01'),
          gender: 'unknown',
          parents: [],
        }),
      ).resolves.toEqual(oneIndividual);
    });
  });

  describe('findAll()', () => {
    it('should return an array of individuals', async () => {
      const individuals = await individualsService.findAll(familyId);
      expect(individuals).toEqual(individualArray);
    });
  });

  describe('findOne()', () => {
    it('should get a single individual', () => {
      expect(
        individualsService.findOne(familyId, individualId),
      ).resolves.toEqual(oneIndividual);
    });
  });

  describe('update()', () => {
    it('should successfully insert an individual', () => {
      expect(
        individualsService.update(familyId, individualId, {
          firstName: 'firstName',
          lastName: 'lastName',
          dateOfBirth: new Date('1970-01-01'),
          dateOfDeath: new Date('1970-01-01'),
          gender: 'unknown',
          parents: [{ id: parentIndividualId }],
        }),
      ).resolves.toEqual(oneIndividual);
    });
  });

  describe('remove()', () => {
    it('should call remove with the passed value', async () => {
      const removeSpy = jest.spyOn(individualsRepository, 'delete');
      const retVal = await individualsService.remove(familyId, individualId);
      expect(removeSpy).toBeCalledWith(individualId);
      expect(retVal).toBeUndefined();
    });
  });
});
