import { Family } from '../families/family.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Individual {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'firstname', nullable: true })
  firstName: string | null;

  @Column({ name: 'lastname', nullable: true })
  lastName: string | null;

  @Column({ name: 'dateofbirth', nullable: true })
  dateOfBirth: Date | null;

  @Column({ name: 'dateofdeath', nullable: true })
  dateOfDeath: Date | null;

  @Column({ nullable: true })
  gender: string | null;

  @ManyToMany(() => Individual)
  @JoinTable()
  parents: Individual[] | null;

  @ManyToOne(() => Family, { onDelete: 'CASCADE' })
  family: Family;
}
