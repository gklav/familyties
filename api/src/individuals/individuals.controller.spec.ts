import { Test, TestingModule } from '@nestjs/testing';
import { UUID } from 'crypto';
import { OwnerGuard } from 'src/families/owner.guard';
import { v4 as uuidv4 } from 'uuid';
import { CreateIndividualDto } from './dto/create-individual.dto';
import { CreateParentDto } from './dto/create-parent.dto';
import { IndividualsController } from './individuals.controller';
import { IndividualsService } from './individuals.service';

const familyId: UUID = uuidv4();

const generatedParentId: UUID = uuidv4();

const createParentDto: CreateParentDto = {
  id: generatedParentId,
};

const createIndividualDto: CreateIndividualDto = {
  firstName: 'firstName',
  lastName: 'lastName',
  dateOfBirth: new Date('1970-01-01'),
  dateOfDeath: new Date('2023-01-01'),
  gender: 'unknown',
  parents: [createParentDto],
};

const generatedId: UUID = uuidv4();

describe('IndividualsController', () => {
  let individualsController: IndividualsController;
  let individualsService: IndividualsService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [IndividualsController],
      providers: [
        IndividualsService,
        {
          provide: IndividualsService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation(
                (familyId: string, individual: CreateIndividualDto) =>
                  Promise.resolve({
                    id: generatedId,
                    ...individual,
                  }),
              ),
            findAll: jest.fn().mockResolvedValue([
              {
                id: generatedId,
                ...createIndividualDto,
              },
              {
                id: '695f7f25-178e-4efb-8f00-2efc7b75336e',
                firstName: 'firstName #2',
                lastName: 'lastName #2',
                dateOfBirth: new Date('1940-01-01'),
                dateOfDeath: new Date('1990-01-01'),
                gender: 'unknown',
                parents: [],
              },
            ]),
            findOne: jest
              .fn()
              .mockImplementation((familyId: string, id: string) =>
                Promise.resolve({
                  id: id,
                  ...createIndividualDto,
                }),
              ),
            update: jest
              .fn()
              .mockImplementation(
                (
                  familyId: string,
                  id: string,
                  individual: CreateIndividualDto,
                ) => Promise.resolve({ id, ...individual }),
              ),
            remove: jest.fn(),
          },
        },
      ],
    })
      .overrideGuard(OwnerGuard)
      .useValue({
        canActivte: async () => {
          return true;
        },
      })
      .compile();

    individualsController = app.get<IndividualsController>(
      IndividualsController,
    );
    individualsService = app.get<IndividualsService>(IndividualsService);
  });

  it('should be defined', () => {
    expect(individualsController).toBeDefined();
  });

  describe('create()', () => {
    it('should create an individual', () => {
      individualsController.create(familyId, createIndividualDto);
      expect(
        individualsController.create(familyId, createIndividualDto),
      ).resolves.toEqual({
        id: generatedId,
        ...createIndividualDto,
      });
      expect(individualsService.create).toHaveBeenCalledWith(
        familyId,
        createIndividualDto,
      );
    });
  });

  describe('findAll()', () => {
    it('should return all individuals', () => {
      individualsController.findAll(familyId);
      expect(individualsService.findAll).toHaveBeenCalledWith(familyId);
    });
  });

  describe('findOne()', () => {
    it('should return a specific individual', () => {
      expect(
        individualsController.findOne(familyId, generatedId),
      ).resolves.toEqual({
        id: generatedId,
        ...createIndividualDto,
      });
      expect(individualsService.findOne).toHaveBeenCalledWith(
        familyId,
        generatedId,
      );
    });
  });

  describe('update()', () => {
    it('should remove a specific individual', () => {
      individualsController.update(familyId, generatedId, createIndividualDto);
      expect(
        individualsController.update(
          familyId,
          generatedId,
          createIndividualDto,
        ),
      ).resolves.toEqual({
        id: generatedId,
        ...createIndividualDto,
      });
      expect(individualsService.update).toHaveBeenCalledWith(
        familyId,
        generatedId,
        createIndividualDto,
      );
    });
  });

  describe('remove()', () => {
    it('should remove a specific individual', () => {
      individualsController.remove(familyId, generatedId);
      expect(individualsService.remove).toHaveBeenCalledWith(
        familyId,
        generatedId,
      );
    });
  });
});
