import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { UUID } from 'crypto';
import { OwnerGuard } from '../families/owner.guard';
import { CreateIndividualDto } from './dto/create-individual.dto';
import { Individual } from './individual.entity';
import { IndividualsService } from './individuals.service';

@UseGuards(OwnerGuard)
@Controller('families/:familyId/individuals')
export class IndividualsController {
  constructor(private individualsService: IndividualsService) {}

  @Post()
  async create(
    @Param('familyId', ParseUUIDPipe) familyId: UUID,
    @Body(ValidationPipe) createIndividualDto: CreateIndividualDto,
  ): Promise<Individual> {
    return await this.individualsService.create(familyId, createIndividualDto);
  }

  @Get()
  async findAll(
    @Param('familyId', ParseUUIDPipe) familyId: UUID,
  ): Promise<Individual[]> {
    return this.individualsService.findAll(familyId);
  }

  @Get(':id')
  async findOne(
    @Param('familyId', ParseUUIDPipe) familyId: UUID,
    @Param('id', ParseUUIDPipe) id: UUID,
  ): Promise<Individual> {
    return await this.individualsService.findOne(familyId, id);
  }

  @Put(':id')
  async update(
    @Param('familyId', ParseUUIDPipe) familyId: UUID,
    @Param('id', ParseUUIDPipe) id: UUID,
    @Body(ValidationPipe) createIndividualDto: CreateIndividualDto,
  ): Promise<Individual> {
    return this.individualsService.update(familyId, id, createIndividualDto);
  }

  @Delete(':id')
  async remove(
    @Param('familyId', ParseUUIDPipe) familyId: UUID,
    @Param('id', ParseUUIDPipe) id: UUID,
  ): Promise<string> {
    await this.individualsService.remove(familyId, id);
    return `Removal of individual ${id}`;
  }
}
