import { Module } from '@nestjs/common';
import { IndividualsController } from './individuals.controller';
import { IndividualsService } from './individuals.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Individual } from './individual.entity';
import { FamiliesModule } from '../families/families.modules';

@Module({
  imports: [TypeOrmModule.forFeature([Individual]), FamiliesModule],
  controllers: [IndividualsController],
  providers: [IndividualsService],
})
export class IndividualsModule {}
