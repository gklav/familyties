import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Individual } from './individual.entity';
import { Repository, DeleteResult } from 'typeorm';
import { CreateIndividualDto } from './dto/create-individual.dto';
import { CreateParentDto } from './dto/create-parent.dto';
@Injectable()
export class IndividualsService {
  constructor(
    @InjectRepository(Individual)
    private readonly individualsRepository: Repository<Individual>,
  ) {}

  async create(
    familyId: string,
    individualDto: CreateIndividualDto,
  ): Promise<Individual> {
    try {
      if (individualDto.parents) {
        await this.parentsCheck(individualDto.parents);
      }

      const { id } = await this.individualsRepository.save({
        ...individualDto,
        family: { id: familyId },
      });

      return await this.individualsRepository.findOne({
        where: { id: id },
        relations: { parents: true },
      });
    } catch (err) {
      throw err;
    }
  }

  async findAll(familyId: string): Promise<Individual[]> {
    try {
      return await this.individualsRepository.find({
        where: {
          family: { id: familyId },
        },
        relations: {
          parents: true,
        },
      });
    } catch (err) {
      throw err;
    }
  }

  async findOne(familyId: string, id: string): Promise<Individual> {
    try {
      const result = await this.individualsRepository.findOne({
        where: { id: id, family: { id: familyId } },
        relations: { parents: true },
      });
      if (result) {
        return result;
      }
      throw new NotFoundException();
    } catch (err) {
      throw err;
    }
  }

  async remove(familyId: string, id: string): Promise<DeleteResult> {
    try {
      const result = await this.individualsRepository.findOne({
        where: { id: id, family: { id: familyId } },
      });
      if (result) {
        await this.individualsRepository.delete(id);
        return;
      }
      throw new NotFoundException();
    } catch (err) {
      throw err;
    }
  }

  async update(
    familyId: string,
    id: string,
    individualDto: CreateIndividualDto,
  ): Promise<Individual> {
    try {
      const result = await this.individualsRepository.findOne({
        where: { id: id, family: { id: familyId } },
      });

      if (result) {
        try {
          if (individualDto.parents) {
            await this.parentsCheck(individualDto.parents);
          }

          const update = Object.assign(result, individualDto);
          const { id } = await this.individualsRepository.save(update);
          return await this.individualsRepository.findOne({
            where: { id: id },
            relations: { parents: true },
          });
        } catch (err) {
          throw err;
        }
      }
      throw new NotFoundException();
    } catch (err) {
      throw err;
    }
  }

  private async parentsCheck(parentsDto: CreateParentDto[]): Promise<boolean> {
    // Check if both parents are differents
    if (parentsDto.length == 2) {
      if (parentsDto[0].id == parentsDto[1].id) {
        throw new BadRequestException('Parents must be differents');
      }
    }

    // Check if both parents exist
    await Promise.all(
      parentsDto.map(async (parentDto) => {
        const parent = await this.individualsRepository.findOne({
          where: { id: parentDto.id },
        });

        if (!parent) {
          throw new NotFoundException(
            'Parent ' + parentDto.id + ' does not exist',
          );
        }
      }),
    );
    return true;
  }
}
