import {
  Body,
  Controller,
  HttpCode,
  Post,
  Request,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { Public } from '../common/decorators/public.decorator';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './local/local-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/register')
  @Public()
  async register(
    @Body(ValidationPipe) userDto: CreateUserDto,
  ): Promise<string> {
    try {
      await this.authService.register(userDto);
      return 'User created';
    } catch (error) {
      throw error;
    }
  }

  @Post('/login')
  @HttpCode(200)
  @Public()
  @UseGuards(LocalAuthGuard)
  async login(@Request() req): Promise<any> {
    return await this.authService.login(req.user);
  }
}
