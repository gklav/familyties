import { Test, TestingModule } from '@nestjs/testing';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { User } from '../users/user.entity';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

const user: User = {
  username: 'username',
  password: 'password',
};

const jwt = 'random_value';

const request = {
  user: user,
};

describe('AuthController', () => {
  let authController: AuthController;
  let authService: AuthService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        AuthService,
        {
          provide: AuthService,
          useValue: {
            validateUser: jest.fn().mockImplementation(() => {
              const { password, ...result } = user;
              Promise.resolve(result);
            }),
            register: jest.fn().mockResolvedValue(user),
            login: jest.fn().mockResolvedValue('access_token: ' + jwt),
          },
        },
      ],
    }).compile();

    authController = app.get<AuthController>(AuthController);
    authService = app.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(authController).toBeDefined();
  });

  describe('register()', () => {
    it('should return the value "User created"', () => {
      authController.register(user as CreateUserDto);
      expect(authController.register(user)).resolves.toEqual('User created');
      expect(authService.register).toHaveBeenCalledWith(user as CreateUserDto);
    });
  });

  describe('login()', () => {
    it('should return an access token', () => {
      authController.login(request);
      expect(authController.login(user)).resolves.toEqual(
        'access_token: ' + jwt,
      );
      expect(authService.login).toHaveBeenCalledWith(request.user);
    });
  });
});
