import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import * as bcrypt from 'bcrypt';
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';

const user: User = {
  username: 'username',
  password: 'password',
};

const jwt = 'random_value';

describe('AuthService', () => {
  let authService: AuthService;
  let usersService: UsersService;
  let jwtService: JwtService;

  beforeAll(async () => {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(user.password, salt);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        JwtService,
        {
          provide: JwtService,
          useValue: {
            sign: jest.fn().mockReturnValue(jwt),
          },
        },
        UsersService,
        {
          provide: UsersService,
          useValue: {
            create: jest.fn().mockImplementation(() => Promise.resolve(user)),
            findOne: jest
              .fn()
              .mockImplementation(() =>
                Promise.resolve({ ...user, password: hash }),
              ),
          },
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    usersService = module.get<UsersService>(UsersService);
    jwtService = module.get<JwtService>(JwtService);
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  describe('validateUser()', () => {
    it('should return a user without the password property', () => {
      authService.validateUser(user.username, user.password);
      expect(
        authService.validateUser(user.username, user.password),
      ).resolves.toEqual({ username: user.username });
      expect(usersService.findOne).toHaveBeenCalledWith(user.username);
    });
  });

  describe('register()', () => {
    it('should retrun a user', () => {
      authService.register(user);
      expect(authService.register(user)).resolves.toEqual(user);
      expect(usersService.create).toHaveBeenCalledWith(user);
    });
  });

  describe('login()', () => {
    it('should return a jwt', () => {
      authService.login(user);
      expect(authService.login(user)).resolves.toEqual({ access_token: jwt });
    });
  });
});
